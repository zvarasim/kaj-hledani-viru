import Vue from 'vue'
import Router from 'vue-router'
import App from './App.vue'
import MainMenu from "@/components/MainMenu";
import Game from "@/components/Game";
import NewGameOptions from "@/components/NewGameOptions";
import Scoreboard from "./components/Scoreboard";

Vue.use(Router);

const router = new Router({
    routes: [
        {
            path: '/',
            component: MainMenu,
        },
        {
            path: '/new',
            component: NewGameOptions,
        },
        {
            path: '/game',
            name: 'game',
            component: Game,
            props: (route) => ({
                ...route.params
            })
        },
        {
            path: '/scoreboard',
            component: Scoreboard,
        }
    ]
});

Array.prototype.shuffle = function() {  // A Prototype extensions that adds a method to randomize element order in array
    for (let i = this.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        const temp = this[i];
        this[i] = this[j];
        this[j] = temp;
    }
};

new Vue({
    el: '#app',
    render: h => h(App),
    router
});
