export default class Square {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Returns array of coordinates of 8 neighbour squares.
     * @returns {Square[]}
     */
    getNeighbourSquares() {
        return [
            new Square(this.x + 1, this.y),
            new Square(this.x - 1, this.y),
            new Square(this.x, this.y + 1),
            new Square(this.x, this.y - 1),
            new Square(this.x + 1, this.y - 1),
            new Square(this.x - 1, this.y + 1),
            new Square(this.x + 1, this.y + 1),
            new Square(this.x - 1, this.y - 1)]
    }

}