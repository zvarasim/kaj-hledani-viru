export default class Options {
    constructor(sizeX, sizeY, virCount) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.virCount = virCount;
    }
}