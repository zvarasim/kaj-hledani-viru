import Element from "@/js/Element";

export default class NumberElement extends Element {

    static images = function() {
        let imgs = [];
        for(let i = 1; i < 9; i++) {
            let img = new Image();
            img.src = require("../../assets/images/numbers/" + `number${i}.jpg`);
            imgs[i] = img;
        }
        return imgs;
    }();

    constructor(number) {
        super();
        this.number = number;
        this.class = "NumberElement";
    }

    getImageFile() {
        return NumberElement.images[this.number];
    }

}