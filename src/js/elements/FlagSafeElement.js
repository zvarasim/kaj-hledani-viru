import UnknownSafeElement from "@/js/elements/UnknownSafeElement";

export default class FlagSafeElement extends UnknownSafeElement {

    static image = function() {
        let img = new Image();
        img.src = require("../../assets/images/" + "flag.png");
        return img;
    }();

    constructor() {
        super();
        this.class = "FlagSafeElement";
    }

    getImageFile() {
        return FlagSafeElement.image;
    }

    handleRightClick(game, square) {
        game.setSquareElement(square, new UnknownSafeElement());
        game.saveGame()
    }

}