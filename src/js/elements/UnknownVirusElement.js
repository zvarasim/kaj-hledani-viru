import Element from "@/js/Element";
import VirusElement from "@/js/elements/VirusElement";

export default class UnknownVirusElement extends Element {

    static image = function() {
        let img = new Image();
        img.src = require("../../assets/images/" + "unknown.jpg");
        return img;
    }();

    constructor() {
        super();
        this.class = "UnknownVirusElement";
    }

    getImageFile() {
        return UnknownVirusElement.image;
    }

    handleClick(game, square) {
        game.loseTheGame(square);
    }

    handleRightClick(game, square) {
        game.putFlag(square, false);
    }

    getRevealed() {
        return new VirusElement()
    }

    isUnknown() {
        return true;
    }
}