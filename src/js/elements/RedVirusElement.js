import VirusElement from "@/js/elements/VirusElement";

export default class RedVirusElement extends VirusElement {

    static image = function() {
        let img = new Image();
        img.src = require("../../assets/images/" + "virusRed.png");
        return img;
    }();

    constructor() {
        super();
        this.class = "RedVirusElement";
    }

    getImageFile() {
        return RedVirusElement.image;
    }

}