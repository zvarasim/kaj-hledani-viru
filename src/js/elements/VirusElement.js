import Element from "@/js/Element";

export default function VirusElement(){
    this.class = "VirusElement";
}
VirusElement.prototype = Element.prototype;

VirusElement.image = function() {
    let img = new Image();
    img.src = require("../../assets/images/" + "virus.png");
    return img;
}();

VirusElement.sound = new Audio(require("../../assets/sounds/" + "virus.mp3"));

VirusElement.prototype.getImageFile = function() {
    return VirusElement.image;
};

VirusElement.prototype.handleClick = function(game, square) {
    VirusElement.sound.play().then();
    console.log("Tapped " + square)
};

VirusElement.prototype.isUnknown = function() {
    return false;
};
