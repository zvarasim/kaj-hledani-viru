import Element from "@/js/Element";

export default class SafeElement extends Element {

    static image = function() {
        let img = new Image();
        img.src = require("../../assets/images/" + "safe.jpg");
        return img;
    }();

    constructor() {
        super();
        this.class = "SafeElement";
    }

    getImageFile() {
        return SafeElement.image;
    }

}