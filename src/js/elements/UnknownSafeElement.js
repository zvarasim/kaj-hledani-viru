import Element from "@/js/Element";
import SafeElement from "@/js/elements/SafeElement";

export default class UnknownSafeElement extends Element {

    static image = function() {
        let img = new Image();
        img.src = require("../../assets/images/" + "unknown.jpg");
        return img;
    }();

    constructor() {
        super();
        this.class = "UnknownSafeElement";
    }

    getImageFile() {
        return UnknownSafeElement.image;
    }

    handleClick(game, square) {
        game.revealSafeFields(square);
        game.saveGame();
        game.checkWin();
    }

    handleRightClick(game, square) {
        game.putFlag(square, true);
    }

    getRevealed() {
        return new SafeElement()
    }

    isUnknown() {
        return true;
    }
}