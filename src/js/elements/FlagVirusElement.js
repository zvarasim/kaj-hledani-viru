import UnknownVirusElement from "@/js/elements/UnknownVirusElement";

export default class FlagVirusElement extends UnknownVirusElement {

    static image = function() {
        let img = new Image();
        img.src = require("../../assets/images/" + "flag.png");
        return img;
    }();

    constructor() {
        super();
        this.class = "FlagVirusElement";
    }

    getImageFile() {
        return FlagVirusElement.image;
    }

    handleRightClick(game, square) {
        game.setSquareElement(square, new UnknownVirusElement());
        game.saveGame()
    }

}