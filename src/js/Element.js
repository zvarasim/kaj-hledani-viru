export default function Element() {
    this.class = this.constructor.name;
}

/**
 * The file handle containing the element image.
 */
Element.prototype.getImageFile = function() {};

/**
 * Defines how a left mouse click should be handled.
 * @param game Current instance of game.
 * @param square Location.
 */
Element.prototype.handleClick = function(game, square) {
    console.log(`Tapped ${square} in ${game}`)
};

/**
 * Defines how a right mouse click should be handled.
 * @param game Current instance of game.
 * @param square Location.
 */
Element.prototype.handleRightClick = function(game, square) {
    console.log(`Right clicked ${square} in ${game}`)
};

/**
 * Defines which element should replace this when revealed.
 * @returns {Element} Replacing element.
 */
Element.prototype.getRevealed = function() {
    return this;
};

/**
 * Defines if the actual content of this element is still unknown.
 * @returns {boolean} True if unknown, false otherwise.
 */
Element.prototype.isUnknown = function() {
    return false;
};
