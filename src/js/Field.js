import UnknownVirusElement from "@/js/elements/UnknownVirusElement";
import UnknownSafeElement from "@/js/elements/UnknownSafeElement";
import NumberElement from "@/js/elements/NumberElement";
import RedVirusElement from "@/js/elements/RedVirusElement";
import SafeElement from "@/js/elements/SafeElement";
import VirusElement from "@/js/elements/VirusElement";
import FlagVirusElement from "@/js/elements/FlagVirusElement";
import FlagSafeElement from "@/js/elements/FlagSafeElement";

export default class Field {

    constructor(options) {
        this.options = options;
        this.field = [[]];
        this.time = new Date().getTime();
    }

    /**
     * Fills empty field with randomly generated elements according to the Options struct.
     */
    generateElements() {

        const elements = [];

        for (let i = 0; i < this.options.sizeX * this.options.sizeY; i++) {
            i < this.options.virCount ? elements.push(new UnknownVirusElement()) : elements.push(new UnknownSafeElement());
        }

        elements.shuffle();

        this.field = new Array(this.options.sizeX);
        for (let x = 0; x < this.options.sizeX; x++) {

            this.field[x] = new Array(this.options.sizeY);

            for (let y = 0; y < this.options.sizeY; y++) {
                this.field[x][y] = elements[x * this.options.sizeY + y];
            }
        }
    }

    /**
     * Returns element at particular square.
     * @param square Location.
     * @returns {Element} Element.
     */
    getElementAt(square) {
        return this.field[square.x][square.y];
    }

    /**
     * Sets element at particular square.
     * @param square Location.
     * @param element Element.
     */
    setElementAt(square, element) {
        this.field[square.x][square.y] = element;
    }

    /**
     * Returns all squares next to particular square that are not out of field boundaries.
     * @param square Location.
     * @returns {[Square]}
     */
    getSquaresNextTo(square) {
        let rawSquares = square.getNeighbourSquares();
        let squares = [];
        for (let rawSquare of rawSquares) {
            if (!this.isSquareOutOfBounds(rawSquare)) {
                squares.push(rawSquare);
            }
        }
        return squares;
    }

    /**
     * Returns number of fields next to particular field that contain virus.
     * @param square Location.
     * @returns {number} Number of viruses.
     */
    getNumberOfVirusesNextTo(square) {
        let count = 0;
        let squares = this.getSquaresNextTo(square);
        for (let square of squares) {
            if (this.getElementAt(square) instanceof UnknownVirusElement) {
                count++;
            }
        }
        return count;
    }

    /**
     * Checks if the particular square is out of field boundaries.
     * @param square Location.
     * @returns {boolean} True if square is out of bounds, false otherwise.
     */
    isSquareOutOfBounds(square) {
        return square.x < 0 || square.y < 0 || this.options.sizeX <= square.x || this.options.sizeY <= square.y;
    }

    // Local Storage
    /**
     * Attempts to initialize new Field instance from json.
     * @param json Json to be parsed.
     * @returns {Field} A newly created instance.
     */
    static fromJson(json) {
        let instance = Object.assign(new Field, JSON.parse(json));

        for (let x = 0; x < instance.options.sizeX; x++) {
            for (let y = 0; y < instance.options.sizeY; y++) {
                let element = instance.field[x][y];
                let assigned = null;
                switch (element.class) {
                    case "NumberElement":
                        assigned = new NumberElement;
                        break;
                    case "RedVirusElement":
                        assigned = new RedVirusElement;
                        break;
                    case "SafeElement":
                        assigned = new SafeElement;
                        break;
                    case "UnknownSafeElement":
                        assigned = new UnknownSafeElement;
                        break;
                    case "UnknownVirusElement":
                        assigned = new UnknownVirusElement;
                        break;
                    case "VirusElement":
                        assigned = new VirusElement;
                        break;
                    case "FlagVirusElement":
                        assigned = new FlagVirusElement();
                        break;
                    case "FlagSafeElement":
                        assigned = new FlagSafeElement();
                        break;
                }
                instance.field[x][y] = Object.assign(assigned, element)
            }
        }

        return instance
    }

}